//
//  ViewController2.swift
//  IntermediateUI
//
//  Created by Rajiv Kulshreshtha on 22/07/18.
//  Copyright © 2018 Rajiv Kulshreshtha. All rights reserved.
//

import UIKit
import SafariServices

class ViewController2: UIViewController, SFSafariViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func showWebContent(_ sender: Any) {
        let url = URL(string: "https://google.com")
        
        //In Safari VC
//        let safariVC = SFSafariViewController(url: url!)
//        safariVC.delegate = self
//
//        present(safariVC, animated: true) {
//            print("Presented!")
//        }
        
        //In Safari App
        UIApplication.shared.open(url!, options: ["":""], completionHandler: nil)
        
    }

    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("Safari Finished!")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
