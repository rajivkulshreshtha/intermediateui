//
//  ViewController.swift
//  IntermediateUI
//
//  Created by Rajiv Kulshreshtha on 21/07/18.
//  Copyright © 2018 Rajiv Kulshreshtha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    let data: [[String]] = [["Item1-1", "Item1-2", "Item1-3", "Item1-4"], ["Item2-1", "Item2-2", "Item2-3", "Item2-4"]]

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControlLabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let codeLabel: UILabel = UILabel(frame: CGRect(x: 20, y: 40, width: 300, height: 20))
        codeLabel.text = "Code Label"
        view.addSubview(codeLabel)

        let codeButton: UIButton = UIButton(frame: CGRect(x: 20, y: 100, width: 300, height: 40))
        codeButton.setTitle("CodeButton", for: .normal)
        codeButton.setTitleColor(UIColor.blue, for: .normal)
        codeButton.backgroundColor = UIColor.lightGray
        codeButton.addTarget(self, action: #selector(codeButtonPress), for: .touchUpInside)
        view.addSubview(codeButton)

    }

    @objc func codeButtonPress() {
        print("Code Button Pressed")
    }


    @IBAction func showAlert(_ sender: Any) {
        let alert: UIAlertController = UIAlertController(title: "Title", message: "Message", preferredStyle: .actionSheet)

        let cancleAction: UIAlertAction = UIAlertAction(title: "Cancle", style: .cancel) { (_: UIAlertAction) in
            print("Cancel Handler")
        }

        let destructiveAction: UIAlertAction = UIAlertAction(title: "Destructive", style: .destructive) { (_: UIAlertAction) in
            print("Destructive Handler")
        }

        let defaultAction: UIAlertAction = UIAlertAction(title: "Default", style: .default) { (_: UIAlertAction) in
            print("Default Handler")
        }

        alert.addAction(cancleAction)
        alert.addAction(destructiveAction)
        alert.addAction(defaultAction)

        self.present(alert, animated: true) {
            print("Alert Handler")
        }
    }

    @IBAction func sliderChange(_ sender: UISlider) {

        progressBar.progress = sender.value
    }

    @IBAction func switchChanged(_ sender: UISwitch) {
        if sender.isOn {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        segmentedControlLabel.text = sender.titleForSegment(at: sender.selectedSegmentIndex)
    }

    //Picker Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return data.count

    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data[component].count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return data[component][row]

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        label.text = data[component][row]
    }
}

